"""Simple example of functions to test CrypTen"""

import crypten
import crypten.communicator as comm
from crypten import mpc
import torch


@mpc.run_multiprocess(world_size=3)
def examine_arithmetic_shares():
    """Print encrypted values of CrypTensor([1, 2, 3])"""
    x_enc = crypten.cryptensor([1, 2, 3], ptype=crypten.mpc.arithmetic)

    rank = comm.get().get_rank()
    print(f"Rank {rank}:\n {x_enc}\n")


if __name__ == "__main__":
    torch.set_num_threads(1)
    examine_arithmetic_shares()