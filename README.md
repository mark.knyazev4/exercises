# Задание 1. Docker

### 1.) Посмотреть [Видео по основам Docker](https://www.youtube.com/watch?v=QF4ZF857m44)  
Видео разделено на несколько частей, из них стоит посмотреть:
* `Вступление` - для чего нужен docker и какие сущности в нём используются
* `Простой пример Hello World` - пример работы c docker, по аналогии нужно будет реализовать следующее задание
* `Что такое docker volume` - можно посмотреть, довольно полезная вещь
* `Что такое docker compose` - также можно посмотреть, в задании не понадобится

### 2.) Запустить скрипт `simple_demonstrations.py`
#### Постановка задачи
Скрипт `simple_demonstrations.py` содержит код с использованием библиотеки [CrypTen](https://github.com/facebookresearch/CrypTen), которая не поддерживает Windows в 
полной мере. Для запуска этого скрипта нужно подготовить Dockerfile, внутри которого нужно скопировать файлы проекта, 
установить необходимые библиотеки `requirements.txt` и библиотеку CrypTen с помощью 
команды `pip install git+https://github.com/facebookresearch/CrypTen.git` и запустить `simple_demonstrations.py`.
На основе этого Dockerfile нужно подготовить docker image, на основе которого запустить docker-container.  
При успешном выполнении скрипта вывод должен быть примерно такой:
```
Rank 2:
 MPCTensor(
        _tensor=tensor([7999580876750696644, 3096582138670470360,  488275603391176392])
        plain_text=HIDDEN
        ptype=ptype.arithmetic
)

Rank 1:
 MPCTensor(
        _tensor=tensor([-5009739668281666378,  4827961876984038856, -6605545607841002709])
        plain_text=HIDDEN
        ptype=ptype.arithmetic
)

Rank 0:
 MPCTensor(
        _tensor=tensor([-2989841208468964730, -7924544015654378144,  6117270004450022925])
        plain_text=HIDDEN
        ptype=ptype.arithmetic
)
```


